import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router';
import VueResource from 'vue-resource'
import store from './store'
import { router, afterRouter } from './router/index'
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import axios from 'axios';//引入文件


Vue.prototype.$ajax = axios;//将axios挂载到Vue实例中的$ajax上面,在项目中的任何位置通过this.$ajax使用
Vue.config.productionTip = false;
Vue.use(iView);
Vue.use(VueRouter);
Vue.use(VueResource)

let tempRouter=[];
let tempAfterRouter = [];//登陆后访问的路由
//是否登录
 router.beforeEach((to, from, next) => {
 	console.log(to.meta)
 	if (to.meta.requireAuth) {
		if (!isEmptyObject(store.state.user)) {
                next();
            } else {
                //如果一直没登陆，此时不加载404页面，没权限访问的一直返回登录页面
                next({
                    name: 'login'
                });
            }
 	}
 	else {
 		next();
 		console.log(3)
 	}	
 })


 function isEmptyObject(obj) {
 	for (var key in obj) {
 		return false;
 	}
 	return true;
 }

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
