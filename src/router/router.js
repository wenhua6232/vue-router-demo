import Main from '@/views/Main'
import { routerComponentsName } from '@/router/routerComponentsName'
//不作为Main组件的子页面展示的页面---单独写
export const loginRouter = {
	path:'/login',
	name:'login',
	meta:{
		title:'Login - 登录',
		requireAuth: false
	},
	component : resolve => {
		require(["@/views/login.vue"],resolve)
	}
}
export const page404 = {
    path: "/*",
    name: "error-404",
    meta: {
        title: "404-页面不存在"
    },
    component: resolve => {
        require(["@/views/error-page/404.vue"], resolve);
    }
};

export const page403 = {
    path: "/403",
    meta: {
        title: "403-权限不足"
    },
    name: "error-403",
    component: resolve => {
        require(["@/views/error-page/403.vue"], resolve);
    }
};

export const page500 = {
    path: "/500",
    meta: {
        title: "500-服务端错误"
    },
    name: "error-500",
    component: resolve => {
        require(["@/views/error-page/500.vue"], resolve);
    }
};
export const otherRouter = {
    path: '/',
    name: 'otherRouter',
	component:Main,
	meta:{
		title:'otherRouter',
		requireAuth: true
	},
    redirect: '/group/thailife',
};
export const appRouter = [
	{
		path:'/group',
		name:'group',
		component:Main,
		children:[
			{
				path:'thailife',
				name:'thailife',
				title:'泰生活',
				component: resolve => { require(['@/views/group/thai-life.vue'], resolve); },				meta:{
					title:'111',
					requireAuth: true
				},
			},
            {
				path:'thaimedical',
				name:'thaimedical',
				title:'泰医养',
				component: resolve => { require(['@/views/group/thai-medical.vue'], resolve); }
			}
		]
	}
]

export const routers = [loginRouter, otherRouter, ...appRouter];