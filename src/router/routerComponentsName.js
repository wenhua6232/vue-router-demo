import Main from '@/views/Main.vue'

const ThaiLife = resolve => { require(['@/views/group/thai-life.vue'], resolve); }
const ThaiMedical = resolve => { require(['@/views/group/thai-medical.vue'], resolve); }

export const routerComponentsName = {
    Main: Main,
    ThaiLife: ThaiLife,
    ThaiMedical: ThaiMedical
}